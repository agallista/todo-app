<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Http\Response;

class TodoController extends Controller
{
   public function add(Request $request)
   {
      $description = $request->input('description');
      $results=app('db')-> insert(
         "insert into Tasks(description,done,insertdate) values('$description',false,now())"
      );
      return new Response(null,201);
   }

   //aggiorno il todo con id passato su url /api/todos/{id}
   public function update(Request $request,$id)
   {
      $description = $request->input('description');
      $done=$request->input('done');
      $results=app('db')-> update(
         //UPDATE TASKS SET description='nuovo todo', done='false' WHERE id=1
         "UPDATE  Tasks SET description='$description', done= $done WHERE id =$id"
      );
      return new Response(null,200);
   }

   public function delete(Request $request,$id)
   {
      $results=app('db')-> delete(
         //UPDATE TASKS SET description='nuovo todo', done='false' WHERE id=1
         "DELETE FROM Tasks WHERE id=$id"
      );
      return new Response(null,204);
   }
}