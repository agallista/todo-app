<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->get('/api/info', function () use ($router) {
    
    return "ciao a tutti belli e brutti";
});

//quando arriva una chiamata in GET su url /api/todos
$router->get('/api/todos', function () use ($router) {
    //carico l'array di todo
    $results=app('db')->select("SELECT * FROM Tasks");
    return $results;
});
//quando arriva una chiamata in POST su url /api/todos
//chiama il metodo add del controller TodoController
//come se facesse TodoController->add(...paramentri)
$router->post('/api/todos', 'TodoController@add');

//quando arriva una chiamata in PUT su url /api/todos
//chiama il metodo add del controller TodoController
//come se facesse TodoController->add(...paramentri)
//il parametro{id} verra passato come parametro al metodo update
//perche quando aggiorno un elemento ho bisogno dell'id
$router->put('/api/todos/{id}', 'TodoController@update');

$router->delete('/api/todos/{id}', 'TodoController@delete');

