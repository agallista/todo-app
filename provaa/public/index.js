//questa funzione mi aggiunge il mio todo
//dentro il db del prof Masetti all'indirizzo 10.12.100.10
//usando il mio web server lumen che fa tante cose belle
async function addTodo(totoDescription){
   var newTodo ={
      description:totoDescription
   }
  
   await fetch('http://localhost:8080/api/todos',{
      method:'POST',
      headers:{'Content-Type':'application/json'},
      body: JSON.stringify(newTodo)
   });

   reloadTodos();
}

async function reloadTodos(){
   var todosResponse = await fetch('http://localhost:8080/api/todos');
   var todoJson=await todosResponse.json();

   var ulList=document.getElementById('todosList');
   ulList.innerHTML="";

   for (var i=0;i< todoJson.length; i++){
      var liElement=document.createElement('li');
      var spanElement=document.createElement('span');
      var removeButton=document.createElement('button');
      removeButton.innerText = 'X';
      //Creo una variabile con l'elemento i-esimo della lista(il singolo todo)
      var todo=todoJson[i];
      //metto la descrizione come contenuto del Li
      spanElement.textContent='#'+todo.id+' '+todo.description;
      spanElement.textContent+= '(';
      if(todo.done){
         spanElement.textContent += 'Completato';
      }else{
         spanElement.textContent+='Da completare';
      }
      spanElement.textContent += ')';
      liElement.appendChild(removeButton);
      liElement.appendChild(spanElement);
   
      ulList.appendChild(liElement);
   }
}

// da qui parte la nostra appa JS
// la parola async dice al browser che
// all'interno di questa funzione viene usata 
// un'operazione asincrona (await)
async function main() {
   console.log("running main");
   // prendo il tag div con id app
   var divApp=document.getElementById("app");
   // creo un tag title 
   var title=document.createElement('h1');
   title.textContent="TODO APP";
   // aggiungo il tag all'interno del div
   divApp.appendChild(title);
   // prendo i todos via HTTP con la funzione fetch
   // N.B la parola await permette di rendere la funzione fetch
   // un'operazione asincrona
   
   var todoJson=await (await fetch('http://localhost:8080/api/todos')).json();

   console.log(todoJson);
   var textInput=document.createElement('input');
   textInput.placeholder='Inserisci un nuovo todo';
   textInput.id='addTodoInput';
   divApp.appendChild(textInput);
   //aggiungo il bottone aggiungi
   var addButton=document.createElement('Button');
   addButton.textContent='Aggiungimi';
   divApp.appendChild(addButton);
   //attacho l'evento click del bottone
   addButton.addEventListener('click',function(){
      var todoDesc=document.getElementById('addTodoInput').value;
      addTodo(todoDesc);
   });



   var ulList=document.createElement('ul');

   ulList.id="todosList";

   divApp.appendChild(ulList);
   
   reloadTodos();



// associo l'evento DOMContentLoaded 
// alla funzione main
document.addEventListener("DOMContentLoaded", function(){
   main();
});